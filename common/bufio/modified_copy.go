package bufio

import (
	"context"
	"net"

	"github.com/sagernet/sing/common"
	N "github.com/sagernet/sing/common/network"
	"github.com/sagernet/sing/common/rw"
	"github.com/sagernet/sing/common/task"
)

type TraficCounter interface {
	Add(n int64)
}

func MCopyConn(ctx context.Context, source net.Conn, destination net.Conn, tc TraficCounter) error {
	return MCopyConnContextList([]context.Context{ctx}, source, destination, tc)
}

func MCopyConnContextList(contextList []context.Context, source net.Conn, destination net.Conn, tc TraficCounter) error {
	var group task.Group
	if _, dstDuplex := common.Cast[rw.WriteCloser](destination); dstDuplex {
		group.Append("upload", func(ctx context.Context) error {
			un, uerr := Copy(destination, source)
			// fmt.Println("upppp isss ", un)
			tc.Add(un)
			err := common.Error(un, uerr)
			if err == nil {
				rw.CloseWrite(destination)
			} else {
				common.Close(destination)
			}
			return err
		})
	} else {
		group.Append("upload", func(ctx context.Context) error {
			defer common.Close(destination)
			return common.Error(Copy(destination, source))
		})
	}
	if _, srcDuplex := common.Cast[rw.WriteCloser](source); srcDuplex {
		group.Append("download", func(ctx context.Context) error {
			dn, derr := Copy(source, destination)
			// fmt.Println("down issssss ", dn)
			tc.Add(dn)
			err := common.Error(dn, derr)
			if err == nil {
				rw.CloseWrite(source)
			} else {
				common.Close(source)
			}
			return err
		})
	} else {
		group.Append("download", func(ctx context.Context) error {
			defer common.Close(source)
			return common.Error(Copy(source, destination))
		})
	}
	group.Cleanup(func() {
		common.Close(source, destination)
	})
	return group.RunContextList(contextList)
}
func MCopyPacketConn(ctx context.Context, source N.PacketConn, destination N.PacketConn, tc TraficCounter) error {
	return MCopyPacketConnContextList([]context.Context{ctx}, source, destination, tc)
}

func MCopyPacketConnContextList(contextList []context.Context, source N.PacketConn, destination N.PacketConn, tc TraficCounter) error {
	var group task.Group
	group.Append("upload", func(ctx context.Context) error {
		un, uerr := CopyPacket(destination, source)
		tc.Add(un)
		return common.Error(un, uerr)
	})
	group.Append("download", func(ctx context.Context) error {
		dn, derr := CopyPacket(source, destination)
		tc.Add(dn)
		return common.Error(dn, derr)
	})
	group.Cleanup(func() {
		common.Close(source, destination)
	})
	group.FastFail()
	return group.RunContextList(contextList)
}
